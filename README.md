rackspace
=========

(A reconstruction of the rackspace gem that was deleted from github).

A simple ruby interface to various Rackspace Cloud APIs. 
Currently, the only implemented API is the Cloud Load Balancer API, but
others can be implemented very easily.

Load Balancers
==============

Using the API is fairly simple, and the methods are largely self-documenting:

```ruby
#!/usr/bin/env ruby
require 'rackspace'
require 'pp'

lbs = Rackspace::LoadBalancers.new()
lbs.authenticate({
	:username => 'user',
	:key      => 'api-key-here'
})

# Pretty-print a list of load balancer names
puts "Load balancers:\n"
pp list = lbs.list()

# ... and the list of nodes
puts "Nodes on #{list[0]}:\n"
pp lbs.list_nodes(list[0])
```

TODO
====

* Implement other APIs/features
* Better documentation