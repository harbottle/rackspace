#!/usr/bin/env ruby

require 'net/http'
require 'uri'
require 'json'

#
# Base class for Rackspace Cloud API Services
#
# This class handles authentication, and all basic
# service features (GET,PUT,DELETE) transparently.
#
class Rackspace
	def initialize
		@@credentials   = {}
		@@authenticated = 0
		@@auth_config   = {}
		@ltype          = nil
		@service        = nil
		@base_uri       = nil
		@region         = 'ORD'
		@name2id        = {}
		@x_list         = {}
		@s2id           = {} # Subitem to ID
		@nfxx           = {} # name field lookup
	end

	# Get/Set region
	def region(region=nil)
		@region = (region ? region : @region)
		return @region
	end

	# Do an actual request, returns a hash representing the JSON result (or do custom processing)
	def do_req(verb,url,body=nil,fun=nil)
		raise 'This method shouldn\'t be called directly' unless (@ltype && @service && @base_uri)

		# We don't need to do this when authenticating (obviously)
		unless url.index('v1.1/auth')
			raise 'You must call authenticate() before anything else' unless @@authenticated

			# Get the service map entries for this service
			raise 'No service map for service "' + @service + '"' unless cfg = @@auth_config['auth']['serviceCatalog'][@service]

			# Get the service URL based on region (TODO: handle snet)
			av_regions  = []
			regioned    = false
			service_url = nil
			cfg.each { |x|
				if (x['region'])
					av_regions.push(x['region'])
					regioned    = true
					service_url = (x['region'] == @region) ? x['publicURL'] : service_url
				elsif (x['publicURL'])
					service_url = x['publicURL']
				end
			}

			# Make sure region is valid (if applicable)
			raise 'Invalid region "' + @region + '" for service "' + @service + 
			      '" available: [' + av_regions.join(',') + "]" if !service_url && regioned
			raise 'Unable to determine service_url for service "' + @service + '"'  if !service_url
		else
			service_url = ''
		end

		# Extract info from the url
		uri  = URI(service_url + url)
		http = Net::HTTP.new(uri.host,uri.port)
		http.use_ssl = true if uri.scheme == 'https'

		# Figure the type of request to make
		case verb
		when 'GET'
			req = Net::HTTP::Get.new(uri.path)
		when 'POST'
			raise "POST requires a body" unless body
			req = Net::HTTP::Post.new(uri.path)
		when 'PUT'
			raise "PUT requires a body" unless body
			req = Net::HTTP::Put.new(uri.path)
		when 'DELETE'
			req = Net::HTTP::Delete.new(uri.path)
		else
			raise 'Invalid HTTP verb: "' + verb + '"'
		end

		# Add Accept
		req.add_field('Accept','application/json')

		# Add the body
		req.content_type = 'application/json'
		req.body         = body

		# Do it (and handle HTTP error responses)
		fun = fun ? fun : proc { |resp| JSON.parse(resp.body) }

		begin
			# Delete and add the token (in case of 'retry')
			req.delete('X-Auth-Token')
			req.add_field('X-Auth-Token',@@auth_config['auth']['token']['id']) if @@authenticated && @@auth_config && !@@auth_config.empty?

			# Send the request, get the response
			resp = http.request(req)

			case resp.code
			when '200', '201', '202'
				fun.call(resp)
			when '400'
				raise 'Bad Request: ' + resp.body
			when '401'
				bod = JSON.parse(resp.body)
				raise Rackspace::ExpiredToken, bod if @@authenticated && @@auth_config && !@@auth_config.empty?
				raise 'Unauthorized: ' + (bod.empty? ? resp.body : (bod['unauthorized'] ? bod['unauthorized']['message'] : bod['message']))
			when '404'
				raise 'Resource not found'
			when '413'
				raise 'Don\'t be a butt-knocker. You\'re sending too many requests.'
			when '422'
				bod = JSON.parse(resp.body)
				raise Rackspace::Retry, bod['message'] if /considered immutable/.match(bod['message'])
				raise bod['message']
			when '500'
				raise 'Internal Error: ' + resp.body
			when '503'
				raise 'Service Unavailable: ' + resp.body
			else
				raise 'Unknown Response: [' + resp.code + '] ' + resp.body
			end
		rescue Rackspace::ExpiredToken
			authenticate(@@credentials)
			retry
		rescue Rackspace::Retry
			sleep(5)
			retry
		end
	end

	#
	# Authenticate to Rackspace (v1.1)
	#
	# credentials is a hash of:
	# {
	#		:username => 'username',
	#		:key      => 'api_key'
	#	}
	#
	# uk should be 1 for UK-based accounts
	#
	def authenticate(credentials, uk=0)
		# Check to make sure we have all our credentials
		raise 'Missing :username' unless credentials[:username]
		raise 'Missing :key'      unless credentials[:key]
		creds = {'credentials' => credentials}.to_json
		@@credentials = credentials

		# Post, and handle the response
		do_req('POST','https://' + ((uk == 1) ? 'lon.' : '') + 'identity.api.rackspacecloud.com/v1.1/auth',creds,proc { |resp|
			@@auth_config   = JSON.parse(resp.body)
			@@authenticated = 1
		})
	end

	# List all 'things' of the given type
	def list
		raise 'This method shouldn\'t be called directly'         unless (@ltype && @service && @base_uri)
		raise 'You must call authenticate() before anything else' unless @@authenticated
		@x_list = {}

		do_req('GET',@base_uri,nil,proc { |resp|
			JSON.parse(resp.body)[@ltype].each { |z|
				@name2id[z['name']] = z['id'].to_s
				@x_list[z['name']]  = get(z['name'])
			}
		})

		return @x_list.keys
	end

	# Get one/all resource(s)
	def get(name=nil,sub_uri=nil)
		# Otherwise, assume the user knows what they want
		raise 'Rackspace::get: ' + @ltype + '/' + name + ' doesn\'t exist' unless !name || (name && @name2id[name])
		uri     = @base_uri
		uri     = uri + '/' + @name2id[name] if name
		uri     = uri + '/' + sub_uri        if sub_uri
		do_req('GET',uri)
	end

	#
	# Create one resource
	#
	# If name is specified, it is assumed that a sub-object is intended
	# If sub_uri is specified it is assumed to be a URI relative to name
	#
	# This update the name -> id mapping for the main resource
	# sub-resources should be handled by the actual API implementation.
	#
	def post(attributes,name=nil,sub_uri=nil)
		raise 'Rackspace::post: name must be specified'       unless (name || name = attributes['name'])
		raise 'Rackspace::post: attributes must be specified' unless attributes
		uri  = @base_uri
		uri  = uri + '/' + @name2id[name] if name
		uri  = uri + '/' + sub_uri        if sub_uri
		do_req('POST',uri,attributes.to_json,proc { |resp|
			data = JSON.parse(resp.body)
			unless sub_uri
				@x_list[name]  = data
				@name2id[name] = @x_list[name][@ltype]['id']
			end
			
			return data
		})
	end

	# Modify one resource
	def put(attributes,name,sub_uri=nil)
		raise 'Rackspace::put: name must be specified'       unless name
		raise 'Rackspace::put: attributes must be specified' unless attributes && attributes[attributes.keys[0]]
		raise 'Rackspace::put: ' + @ltype + '/' + name + ' doesn\'t exist' unless @name2id[name]
		uri  = @base_uri + '/' + @name2id[name]
		uri  = uri + '/' + sub_uri if sub_uri
		do_req('PUT',uri,attributes.to_json,proc { |resp|
			# Handle name changes
			attributes = attributes[attributes.keys[0]]
			if (!sub_uri && attributes['name'] && attributes['name'] != name)
				@x_list[attributes['name']]  = @x_list[name]
				@name2id[attributes['name']] = @name2id[name]
				@name2id.delete(name)
				@x_list.delete(name)
				name = attributes['name']
			end

			# Update attributes
			attributes.keys.each { |k| @x_list[name][@ltype.chop][k] = attributes[k] } unless sub_uri
		})
	end

	# Delete one resource
	def delete(name,sub_uri=nil)
		raise 'Rackspace::delete: ' + @ltype + '/' + name + ' doesn\'t exist' unless (name && @name2id[name])
		uri = @base_uri + '/' + @name2id[name]
		uri = uri + '/' + sub_uri if sub_uri
		do_req('DELETE',uri,nil,proc { |resp| 
			unless sub_uri
				@name2id.delete(name)
				@x_list.delete(name)
			end
		})
	end

	# Get subitems
	def get_subitems(name,type,namefield)
		@s2id[name] = { type => {} }
		@nfxx[type] = namefield unless @nfxx[type]
		items = @x_list[name][@ltype.chop][type]
		items.each { |item| @s2id[name][type][item[namefield]] = item['id'].to_s }
		return items
	end

	# Create subitems
	def create_subitems(name,type,attributes)
		resp = post({ type => attributes },name,type.downcase)[type]
		resp.each { |item|
			@s2id[name][type][item[@nfxx[type]]] = item['id'].to_s
			@x_list[name][@ltype.chop][type].push(item)
		}
		return resp
	end

	# Update subitems
	def update_subitems(name,type,allowed_attribs,items)
		items.keys.each { |item|
			raise "No such #{type.chop}: #{item} on #{name}" unless @s2id[name][type][item]
			items[item].keys.each { |k| items[item].delete(k) unless allowed_attribs.index(k) }
			put({ type.chop => items[item] },name,type.downcase + '/' + @s2id[name][type][item])

			# Update the local copy
			@x_list[name][@ltype.chop][type].each { |xitem|
				if xitem[@nfxx[type]] == item
					items[item].keys.each { |k| xitem[k] = items[item][k] if xitem[k] && items[item][k] }
				end
			}
		}
	end

	# Delete subitems
	def delete_subitems(name,type,items)
		items.each { |item|
			if @s2id[name][type][item]
				delete(name,type.downcase + '/' + @s2id[name][type][item])
				@s2id[name][type].delete(item)
				@x_list[name][@ltype.chop][type].each { |xitem|
					@x_list[name][@ltype.chop][type].delete(xitem) if xitem[@nfxx[type]] == item
				}
			end
		}
	end
end

class Rackspace::ExpiredToken < Exception 
end

class Rackspace::Retry < Exception
end

# vi:set ts=2:
