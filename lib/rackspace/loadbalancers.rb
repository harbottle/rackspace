#!/usr/bin/env ruby

#
# Interface to Rackspace's Cloud Load Balancer API
#
# TODO: SSL Termination, Virtual IPs, ACLs, Usage Reports, Health Monitors, 
# Connection Persistence/Logging/Throttling, Content Caching
#
class Rackspace::LoadBalancers < Rackspace
	def initialize
		super
		@ltype    = 'loadBalancers'      # for list()
		@service  = 'cloudLoadBalancers' # from @@auth_config
		@base_uri = '/loadbalancers'     # for Rackspace::get/put/etc.
		@metadata = {}
	end

	# Get load balancer details
	def get_details(name)
		if @x_list[name][@ltype.chop]
			# Get metadata for this lb
			md = get(name,'metadata')
			@metadata[name] = { 'lb' => md[md.keys[0]] }
		end

		@x_list[name][@ltype.chop]
	end

	# Create a load balancer
	def create(attributes)
		post(attributes)
	end

	# Update load balancer attributes
	def update(name,attributes)
		allowed_attribs = [ 'name', 'algorithm', 'protocol', 'port' ]
		attributes.keys.each { |k| attributes.delete(k) unless allowed_attribs.index(k) }
		put({ @ltype.chop => attributes },name)
	end

	# Get load balancer stats
	def stats(name)
		get(name,'stats')
	end

	# Get Allowed Domains
	def get_allowed_domains
		out = []
		ad = get(nil,'alloweddomains')
		ad[ad.keys[0]].each { |x| out.push(x['allowedDomain']['name']) }
		return out
	end

	# List nodes
	def list_nodes(name)
		@metadata[name] = { 'lb' => {} } unless @metadata[name]
		@metadata[name]['nodes'] = {}
		nodes = get_subitems(name,'nodes','address')
		nodes.each { |node|
			# Get metadata for this node
			md = get(name,'nodes/' + @s2id[name]['nodes'][node['address']] + '/metadata')
			@metadata[name]['nodes'][node['address']] = md[md.keys[0]]
		}
	end

	# Add nodes
	def add_nodes(name,nodes)
		nodes.each { |node|
			raise 'You must specify an address and a port when adding a node' unless node[:address] && node[:port]
			node[:condition] = 'ENABLED' unless node[:condition]
			node[:type]      = 'PRIMARY' unless node[:type]
		}

		create_subitems(name,'nodes',nodes)
	end

	# Modify nodes
	def update_nodes(name,nodes)
		update_subitems(name,'nodes',[ 'condition', 'weight', 'type' ],nodes)
	end

	# Delete nodes
	def delete_nodes(name,nodes)
		delete_subitems(name,'nodes',nodes)
	end

	# Get error page
	def get_error_page(name)
		get(name,'errorpage')['content']
	end

	# Set error page
	def set_error_page(name,content)
		put({ :content => content },name,'errorpage')
	end

	# Delete error page
	def delete_error_page(name)
		delete(name,'errorpage')
	end

	# List Virtual IPs
	def list_vips(name)
		get_subitems(name,'virtualIps','address')
	end

	# Add Virtual IPs
	def add_vips(name,vips)
		vips.each { |vip|
			raise 'You must specify the ipVersion and type when adding a virtual IP' unless vip[:ipVersion] && vip[:type]
		}

		create_subitems(name,'virtualIps',vips)
	end

	# Delete Virtual IPs
	def delete_vips(name,vips)
		delete_subitems(name,'virtualIps',vips)
	end

	# Get metadata
	def get_metadata(name,node=nil)
		raise "No such node: #{node} on #{name}" unless !node || @s2id[name]['nodes'][node]
		mda = node ? @metadata[name]['nodes'][node] : @metadata[name]['lb']
		out = {}
		mda.each { |md| out[md['key']] = md['value'] }
		return out
	end

	# Add metadata
	def add_metadata(name,values,node=nil)
		raise "No such node: #{node} on #{name}" unless !node || @s2id[name]['nodes'][node]
		resp = post({ 'metadata' => values },name,(node ? 'nodes/' + @s2id[name]['nodes'][node] + '/' : '') + 'metadata')
		resp['metadata'].each { |md|
			if node
				@metadata[name]['nodes'][node].push(md)
			else
				@metadata[name]['lb'].push(md)
			end
		}
	end

	# Update metadata
	def update_metadata(name,values,node=nil)
		raise "No such node: #{node} on #{name}" unless !node || @s2id[name]['nodes'][node]
		mda = (node ? @metadata[name]['nodes'][node] : @metadata[name]['lb'])

		values.keys.each { |k|
			md = nil
			mda.each { |mdx| md = mdx if mdx['key'] == k }
			if md
				put({ 'meta' => { 'value' => values[k] }},name,
				    (node ? 'nodes/' + @s2id[name]['nodes'][node] + '/' : '') + 'metadata/' + md['id'].to_s)
				md['value'] = values[k]
			end
		}
	end

	# Delete metadata
	def delete_metadata(name,key,node=nil)
		raise "No such node: #{node} on #{name}" unless !node || @s2id[name]['nodes'][node]
		md  = nil
		mda = (node ? @metadata[name]['nodes'][node] : @metadata[name]['lb'])
		mda.each { |mdx| md = mdx if mdx['key'] == key }
		if md
			delete(name,(node ? 'nodes/' + @s2id[name]['nodes'][node] + '/' : '') + 'metadata/' + md['id'].to_s)
			mda.delete(md)
		end
	end
end

# vi:set ts=2:
