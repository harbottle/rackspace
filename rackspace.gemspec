# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "rackspace"
  spec.version       = "0.1.2"
  spec.authors       = ["Tim Hentenaar", "Richard Grainger"]
  spec.email         = ["grainger@gmail.com"]

  spec.summary       = "Simple interface to Rackspace's Cloud APIs"
  spec.description   = "\tRuby interface to various Rackspace Cloud APIs.\n\n\tCurrently only the Cloud Load Balancers API is supported. See the README for more\n\tdetails.\n"
  spec.homepage      = "https://gitlab.com/harbottle/rackspace"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.require_paths << "lib"

  spec.add_dependency("json", ">=0")

end